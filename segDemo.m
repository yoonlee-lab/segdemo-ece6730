%%%%%%%%%%%%%%
% Parameters %
%%%%%%%%%%%%%%
SHOW_DETAIL = false; % Show change in each iteration (true), vs. change from t=0 (false)

% Domain dimensions
NUM_ROWS = 30;
NUM_COLS = NUM_ROWS; % square grid

% Population parameters
PROB_EMPTY = 0.05; % probability that a cell is empty
POP_RATIO = 1; % population bias: ratio of group "A" ("+1") size to "B" ("-1") size
HAPPY_THRESHOLD = 0.3

% Maximum number of time steps
MIN_DIM = min ([NUM_ROWS NUM_COLS]);
MAX_TIME = round (MIN_DIM * log (MIN_DIM));

% Make movie; see also: https://www.mathworks.com/help/matlab/ref/getframe.html
MOVIE = true;

% Movie output file (set to empty string for none)
SAVE_GIF = sprintf ('sim-%dx%d-%f-%f-%f.gif', NUM_ROWS, NUM_COLS, PROB_EMPTY, POP_RATIO, HAPPY_THRESHOLD);

%%%%%%%%%%%%%
% Main Demo %
%%%%%%%%%%%%%

% Initial grid
X_0 = createDomain (NUM_ROWS, NUM_COLS, PROB_EMPTY, POP_RATIO);

% Draw initial grid
figure (1); clf;
n_A = length (find (X_0 > 0)); % # in group "A" ("+1")
n_B = length (find (X_0 < 0)); % # in group "B" ("-1")
showDomain (X_0, sprintf ('Initial [%d:%d]', n_A, n_B));
pause

if MOVIE
    display ('Creating frames for an animated movie (`Frames` array)...');
    Frames(MAX_TIME+1) = struct('cdata',[],'colormap',[]);
end

% Main simulation (time-stepping) loop
X_t = X_0;
for t=1:MAX_TIME,

  display (sprintf ('=== Time step: %d ===', t))

  % Identify the type of each household
  A_t = (X_t > 0); % Group "A" ("+1")
  B_t = (X_t < 0); % Group "B" ("-1")
  
  % Count the types in each neighborhood
  N_A_t = sumNeighborhoods (A_t);
  N_B_t = sumNeighborhoods (B_t);
  N_t = N_A_t + N_B_t;
  
  % Convert counts to densities
  R_A_t = N_A_t ./ N_t;
  R_A_t(isnan (R_A_t)) = 0;
  R_B_t = N_B_t ./ N_t;
  R_B_t(isnan (R_B_t)) = 0;
  
  % Measure the happiness of each household
  L_t = ((A_t .* R_A_t) + (B_t .* R_B_t)); % fraction of same type
  H_t = L_t - HAPPY_THRESHOLD; % H_t >= 0 ==> happy enough

  % Determine who is unhappy with their neighborhood ("movable")
  M_t = (X_t ~= 0) & (H_t < 0);

  % Move the "A" ("+1") group first
  F_A_t = getFree (X_t) .* (R_A_t >= HAPPY_THRESHOLD); % desirable locations
  Y_t = moveGroup (X_t, A_t .* M_t, F_A_t);

  % Move the "B" ("-1") group next
  F_B_t = getFree (Y_t) .* (R_B_t >= HAPPY_THRESHOLD); % desirable locations
  Z_t = moveGroup (Y_t, -B_t .* M_t, F_B_t);

  n_moved = sum (sum (abs (abs (Z_t) - abs (X_t)))) / 2;
  display (sprintf ('  %d cells were movable.', sum (sum (M_t))));
  display (sprintf ('  %d cells moved.', n_moved));

  figure (1); clf;
  if SHOW_DETAIL,
    subplot (2, 2, 1); showDomain (X_t, sprintf ('Population: t=%d', t), [-1, 1]);
  else
    n_A = length (find (X_0 == 1)); n_B = length (find (X_0 == -1));
    subplot (2, 2, 1); showDomain (X_0, sprintf ('Initial [%d:%d]: t=0', n_A, n_B), [-1, 1]);
  end      
  n_A = length (find (X_t == 1)); n_B = length (find (X_t == -1));
  subplot (2, 2, 2); showDomain (H_t, sprintf ('Neighborhood color [%d:%d]', n_A, n_B), [-1, 1]);
  n_M = length (find (M_t ~= 0));
  subplot (2, 2, 3); showDomain (M_t, sprintf ('Movable [%d]', n_M), [-1, 1]);
  
  if MOVIE & (t == 1)
    subplot (2, 2, 4); showDomain (X_0, sprintf ('Population: t=%d', 0), [-1, 1]);
    Frames(1) = getframe(gcf);
  end      
  subplot (2, 2, 4); showDomain (Z_t, sprintf ('New population: t=%d', t), [-1, 1]);
  
  if MOVIE
      Frames(t+1) = getframe(gcf);
  end

  X_t = Z_t;
  if n_moved == 0,
    display ('*** END: Reached a steady state (or, if you prefer, an impasse). ***')
    break;
  end
end

if MOVIE & (t < MAX_TIME+1)
    Frames = Frames(1:t+1); % truncate
end

if MOVIE & ~strcmp(SAVE_GIF, '')
    display (sprintf ('Saving movie to `%s`...', SAVE_GIF));
    % Adapted from https://www.mathworks.com/matlabcentral/answers/94495-how-can-i-create-animated-gif-images-in-matlab
    for t = 1:length (Frames)
      im = frame2im (Frames(t));
      [imind, cm] = rgb2ind (im, 256); 
      if t == 1
        imwrite(imind, cm, SAVE_GIF, 'gif', 'Loopcount', inf, 'DelayTime', 2); 
      else 
        imwrite(imind, cm, SAVE_GIF, 'gif', 'WriteMode', 'append', 'DelayTime', 2);
      end           
    end
    display ('Done saving movie.');
end

% eof
