function C = sumNeighborhoods (X, varargin)
  [m, n] = size (X);
  I = (2:(m-1));
  J = (2:(n-1));
  
  % Measure the "color" of the neighborhood centered at every (i, j)
  C = zeros (size (X));
  C(I, J) = C(I, J) + X(I-1, J-1) + X(I-1, J) + X(I-1, J+1);
  C(I, J) = C(I, J) + X(I,   J-1) +             X(I,   J+1);
  C(I, J) = C(I, J) + X(I+1, J-1) + X(I+1, J) + X(I+1, J+1);
  
  if length(varargin) & varargin{1} == 'include_center'
      C(I, J) = C(I, J) + X(I, J);
  end
end

% eof
