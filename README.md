# Schelling model, for in-class segregation demo #

Based on modeing ideas that appear in:

Thomas C. Schelling. _Micromotives and Macrobehaviors_. W. W. Norton & Company, 2006. [Amazon Kindle Edition](http://www.amazon.com/Micromotives-Macrobehavior-Thomas-C-Schelling-ebook/dp/B002MB968C/ref=tmm_kin_swatch_0?_encoding=UTF8&sr=&qid=)
